# Arké

Projet d'installation sur l'exploration des formes structurelles du vivant. 


## Description
Ce projet poursuit l'axe développé dans l'installation [Post Carbon Framework](https://jeromeabel.net/workshop/post-carbon-framework/), celui de confronter une structure architecturale qui s'inspire du vivant aux regards des machines.

Le volume de la sculpture principale, sujet de l'observation des machines, sera l'équivalent de vingt homo sapiens, une présence qui nous confronte autant que possible à nos dimensions individuelles. Le développement des dessins préparatoires oriente le projet vers une forme hybride ,une sculpture de sable, de vase, de béton, de métal, qui mélange l'animal, le végétal et le minéral. L'idée de départ était de partir d'une forme imposante, d'un être vivant qui aurait pu existé. Comme avec Post Carbon Framework, l'observateur se place dans un contexte temporel qui est dissocié, distancié du sujet.

Les machines seraient de mini-robots qui parcourent les formes de la sculpture, dans un réseau filaire qui forme une autre structure, médiatique, régulière.


## Dessins préparatoires

### Étude 01
<img src="dessins/etude-01.jpg" width="800px">

### Étude 02
<img src="dessins/etude-02.jpg" width="800px">

### Étude 03
<img src="dessins/etude-03.jpg" width="800px">

### Étude 04
<img src="dessins/etude-04.jpg" width="800px">

### Étude 05
<img src="dessins/etude-05.jpg" width="800px">


## Recherches
Le jeu de cette sculpture est de faire dialoguer le réseau de lumières des machines avec la complexité de la forme observée, ces parts d'ombres et d'inconnu. La lumière est l'outil principal de l'observation car elle véhicule des informations sur la composition de la matière grâce à la tomographie, la spectographie.

Ici, les machines solitaires explorent une partie de la forme. Bloquées par un mécanisme linéaire, elles ne peuvent pas aboutir à avoir une image totale. La lumière est partout, mais l'observateur lui est circoncis dans un espace-temps qui lui est propre. Si ici la sculpture semble échapper à l'observation, son histoire reste un mystère, le bal des lumières, notre capacité à chercher est un mouvement esthétique.

